{Check It!|assessment}(code-output-compare-1381823458)
{Check It!|assessment}(code-output-compare-2304283082)
{Check It!|assessment}(code-output-compare-3517035658)
{Check It!|assessment}(code-output-compare-591657036)
{Check It!|assessment}(test-2151823409)
{Check It!|assessment}(fill-in-the-blanks-220698338)
{Check It!|assessment}(fill-in-the-blanks-2381378796)
{Check It!|assessment}(multiple-choice-672826359)
{Check It!|assessment}(multiple-choice-3595402755)
{Submit Answer!|assessment}(free-text-728562796)
{Submit Answer!|assessment}(free-text-3616491314)


![images/ffc](images/ffc.bmp)
![images/ffc](images/ffc.gif)
![images/ffc](images/ffc.jpg)
![images/ffc](images/ffc.png)

## Available style
The later sections in this Guide show you the available markdown commands for Guides.

## Starting Guides

Codio Guides has 2 modes :

1. The Guides Editor - Tools->Guides->Edit menu item
1. The Guides Player - Tools->Guides->Play menu item

If you have not yet started work on a Guide, simply select the Edit option and you can start immediately.

## Documentation
Documentation can be found at https://codio.com/docs/ide/tools/guides/

If you find any anomalies, and there will be some due to the many changes we are currently making to Guides, then please email ijobling@codio.com with details and we will get this updated quickly.

